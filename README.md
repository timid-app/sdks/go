# TIMID SDK

Go SDK for the TIMIT API.

## Import
```go
    import timid "gitlab.com/timid-app/sdks/go"
```

## Usage