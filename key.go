package timid

import "encoding/json"

const detailsAPI string = "/details"

// Key defines the structure of an API key
type Key struct {
	Name        string                 `json:"name"`
	Key         string                 `json:"key"`
	Service     string                 `json:"service"`
	Active      bool                   `json:"active"`
	User        string                 `json:"user"`
	Permissions []string               `json:"permissions"`
	Request     []string               `json:"request"`
	Details     map[string]interface{} `json:"details"`
}

// Use a key to get details
func (k *Key) Use() error {

	// Build the request body
	packRequest, _ := json.Marshal(k.Request)
	requestBody := []byte(`{"request":` + string(packRequest) + `}`)

	// Build the headers
	headers := map[string]string{
		"X-Key":     k.Key,
		"X-Service": k.Service,
		"X-Email":   k.User,
	}

	// Send the request
	response, err := request("POST", detailsAPI, requestBody, headers)
	if err != nil {
		// @TODO: handle error
	}

	// Handle the response
	k.Details = response.Details

	return nil
}
