package timid

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
)

const api string = "https://api.timid.app"

// Response structure of the the API reponse
type Response struct {
	Status  string                 `json:"status"`
	Message string                 `json:"message"`
	Token   string                 `json:"token"`
	Details map[string]interface{} `json:"details"`
}

// Make a request to the API and return the results
func request(method string, endpoint string, body []byte, headers map[string]string) (*Response, error) {

	// Define the rqust url
	url := api + endpoint

	// Create a new request
	req, err := http.NewRequest(method, url, bytes.NewBuffer(body))
	if err != nil {
		// @TODO: handle error
	}

	// Add the headers
	if len(headers) > 0 {
		for header, value := range headers {
			req.Header.Add(header, value)
		}
	}

	// Send the request
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		// @TODO: handle error
	}
	defer res.Body.Close()

	// Get the response body
	body, err = ioutil.ReadAll(res.Body)
	if err != nil {
		// @TODO: handle error
	}

	// Open the response
	response := new(Response)
	err = json.Unmarshal(body, response)
	if err != nil {
		// @TODO: handle the error
	}

	return response, nil
}
