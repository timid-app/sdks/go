package timid

// User definition of a TIMID user
type User struct{}

// NewUser creates a new TIMID user
func NewUser(email string, password string) (*User, error) {
	return nil, nil
}

// Login a user
func Login(email string, password string) (*User, error) {
	return nil, nil
}

// Logout a user
func (u *User) Logout() error {
	return nil
}

// Profile gets the users profile
func (u *User) Profile() error {
	return nil
}

// Update updates a users profile
func (u *User) Update() error {
	return nil
}

// Remove deletes a users profile and associated API keys
func (u *User) Remove() error {
	return nil
}
